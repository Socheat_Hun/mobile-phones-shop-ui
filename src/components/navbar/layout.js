import { Content } from "./contents"
import { Box } from "./box";

export const Layout = ({ children }) => (
  <Box
    css={{
      maxW: "100%",
      
    }}
  >
    {children}
    {/* <Content /> */}
  </Box>
);