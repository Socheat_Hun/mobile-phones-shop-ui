import { Navbar, Link, Text, Avatar, Dropdown, Input } from "@nextui-org/react";
import { Layout } from "./layout";
import { AcmeLogo } from "./logo";
import { useRouter } from "next/router";

const collapseItems = [
    "Profile",
    "Dashboard",
    "Activity",
    "Analytics",
    "System",
    "Deployments",
    "My Settings",
    "Team Settings",
    "Help & Feedback",
    "Log Out",
];


export const Navbars = ({ children }) => {
    var {pathname} = useRouter();

    return (
        <Layout>
            <Navbar shouldHideOnScroll isBordered variant="sticky"
                css={{
                    background:'White',
                    "@lg": {

                    }

                }}
            >
                <style>
                    {`
            :root{
              --nextui--navbarHeight: 60px;
              --nextui--navbarBorderWeight:none
            }
          `}
                </style>
                <Navbar.Toggle showIn="xs" />
                <Navbar.Brand
                    css={{
                        "@xs": {
                        },
                        "@lg": {
                            h: '10px'
                        }
                    }}
                >
                    <Text hideIn="xs">
                    <AcmeLogo/>
                    </Text>
                    
                    <Text b color="inherit" css={{ mr: "$11" }} hideIn="xs">
                        ACME
                    </Text>
                    <Navbar.Content hideIn="xs" variant="default">
                        <Navbar.Link isActive href="#"> Dashboard</Navbar.Link>
                        <Navbar.Link href="#">Team</Navbar.Link>
                        <Navbar.Link href="#">Activity</Navbar.Link>
                        <Navbar.Link href="#">Settings</Navbar.Link>
                    </Navbar.Content>
                </Navbar.Brand>
                <Navbar.Content
                    css={{
                        "@xsMax": {
                            w: "100%",
                            jc: "space-between",
                        },
                    }}
                >
                    <Navbar.Item
                        css={{
                            "@xsMax": {
                                w: "100%",
                                jc: "center",
                            },
                        }}
                    >
                        <Input
                            clearable

                            contentLeftStyling={false}
                            css={{
                                w: "100%",
                                "@xsMax": {
                                    mw: "300px",
                                },
                                "& .nextui-input-content--left": {
                                    h: "100%",
                                    ml: "$4",
                                    dflex: "center",
                                },
                            }}
                            placeholder="Search..."
                        />
                    </Navbar.Item>
                    <Dropdown placement="bottom-right">
                        <Navbar.Item>
                            <Dropdown.Trigger>
                                <Avatar
                                    bordered
                                    as="button"
                                    color="secondary"
                                    size="md"
                                    src="https://i.pravatar.cc/150?u=a042581f4e29026704d"
                                />
                            </Dropdown.Trigger>
                        </Navbar.Item>
                        <Dropdown.Menu
                            aria-label="User menu actions"
                            color="secondary"
                            onAction={(actionKey) => console.log({ actionKey })}
                        >
                            <Dropdown.Item key="profile" css={{ height: "$18" }}>
                                <Text b color="inherit" css={{ d: "flex" }}>
                                    Signed in as
                                </Text>
                                <Text b color="inherit" css={{ d: "flex" }}>
                                    zoey@example.com
                                </Text>
                            </Dropdown.Item>
                            <Dropdown.Item key="settings" withDivider>
                                My Settings
                            </Dropdown.Item>
                            <Dropdown.Item key="team_settings">Team Settings</Dropdown.Item>
                            <Dropdown.Item key="analytics" withDivider>
                                Analytics
                            </Dropdown.Item>
                            <Dropdown.Item key="system">System</Dropdown.Item>
                            <Dropdown.Item key="configurations">Configurations</Dropdown.Item>
                            <Dropdown.Item key="help_and_feedback" withDivider>
                                Help & Feedback
                            </Dropdown.Item>
                            <Dropdown.Item key="logout" withDivider color="error">
                                Log Out
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Navbar.Content>
                <Navbar.Collapse>
                    {collapseItems.map((item, index) => (
                        <Navbar.CollapseItem
                            key={item}
                            activeColor="secondary"
                            css={{
                                color: index === collapseItems.length - 1 ? "$error" : "",
                            }}
                            isActive={index === 2}
                        >
                            <Link
                                color="inherit"
                                css={{
                                    minWidth: "100%",
                                }}
                                href="#"
                            >
                                {item}
                            </Link>
                        </Navbar.CollapseItem>
                    ))}
                </Navbar.Collapse>
            </Navbar>
            {children}
        </Layout>
    )
}


