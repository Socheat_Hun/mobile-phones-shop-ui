import { Card, Carousel, Col, Row, Typography } from 'antd';
const { Title } = Typography;

const contentStyle = {
  height: '80px',
};

const style = {
  padding: '0px 0',
  textAlign: 'center',
};

export const Slider = () => {
    return (
        <Card className="card-body" style={{ marginLeft: '15%', marginRight: '15%', marginTop: '10px' }} bodyStyle={{ padding: "10px" }}>
            <Carousel effect="fade" autoplay>
                <div>
                    <Row
                        gutter={{
                            xs: 8,
                            sm: 16,
                            md: 24,
                            lg: 32,
                        }}
                    >
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://coolstuf.com.pg/wp-content/uploads/2022/10/iPhone-14-pro-model-1.jpg' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">iPhone 14 pro max</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://i05.appmifile.com/374_operator_uk/20/04/2022/2795582230669a9a84a95c776c3e1729!500x500.png' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">Samsung Galaxy S6</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://i0.wp.com/platform-decentral.com/wp-content/uploads/2021/03/iPhone-13-Pro-with-Hole-Punch-Camera.122.png?resize=740%2C1184&ssl=1' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">iPhone 12 pro</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://www.phonebd.net/storage/mobiles/0162ff9e.png' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">iPhone 14 pro max Gray</Title>
                                </Row>
                            </div>
                        </Col>
                    </Row>
                </div>
                <div>
                    <Row
                        gutter={{
                            xs: 8,
                            sm: 16,
                            md: 24,
                            lg: 32,
                        }}
                    >
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://www.smart.com.kh/media/2022/05/reno7z_rainbow-600x600.png' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">Oppo Reno 7</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://cf.shopee.ph/file/31b393a8496c96ffdea2832dcd410e43' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">Oppo Model last</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://manikmobile.com/wp-content/uploads/2022/02/OPPO-2325-A55-6128-GB-RAINBOW-BLUE-e1645268965478-1200x1200.png' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">Oppo Reno 9</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://www.themobileindian.com/wp-content/uploads/2021/12/Oppo-find-N.png' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">Oppo Model 2023</Title>
                                </Row>
                            </div>
                        </Col>
                    </Row>          </div>
                <div>
                    <Row
                        gutter={{
                            xs: 8,
                            sm: 16,
                            md: 24,
                            lg: 32,
                        }}
                    >
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://m-cdn.phonearena.com/images/articles/387194-image/motorola-edge-plus-2022.jpg' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">Sony mini max</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjxqE528B6C33kM60ds--iZsokyd4dBodl5LsxUG8WYgF7CBaY9ysdrIqieQnESke3z0A&usqp=CAU' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">Sony far S6</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSvuMe6-NS5Rce9EaI1mUSNbftc_pQyDYHiY-TYU3MoBNZTm9umQ7JpybbSTcP5OkJUQQ&usqp=CAU' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">Sony EwQ pro</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://dc.watch.impress.co.jp/img/dcw/docs/1361/046/001_s.jpg' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">Sony max Gray</Title>
                                </Row>
                            </div>
                        </Col>
                    </Row>          </div>
                <div>
                    <Row
                        gutter={{
                            xs: 8,
                            sm: 16,
                            md: 24,
                            lg: 32,
                        }}
                    >
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://cdn.appleosophy.com/2022/10/M2-iPadPro.png' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">iPhone 14 pro max</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://www.apple.com/newsroom/images/product/ipad/standard/Apple-iPad-10th-gen-yellow-2up-221018_big.jpg.small.jpg' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">Samsung Galaxy S6</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://cdn.shoplightspeed.com/shops/652975/files/39871485/1652x1652x1/apple-smart-keyboard-for-ipad.jpg' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">iPhone 12 pro</Title>
                                </Row>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div style={style}>
                                <Row className="pupImg">
                                    <img style={contentStyle} src='https://t-mobile.scene7.com/is/image/Tmusprod/Apple-iPad-10th-gen-Blue-rightimage' alt="Logo" />
                                </Row>
                                <Row className="pubTitle">
                                    <Title level={5} className="pubH5">iPhone 14 pro max Gray</Title>
                                </Row>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Carousel>
        </Card>
    );
}