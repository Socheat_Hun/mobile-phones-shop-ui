import { Button, Result, Row } from 'antd';
import { useRouter } from 'next/router';


const PageNotfound = () => {
    const router = useRouter();
    return (
        <Row align={'middle'} style={{ height: '100vh',justifyContent:'center' }}>
            <Result
                status="404"
                title="404"
                subTitle="Sorry, the page you visited does not exist."
                extra={<Button type="primary" onClick={() => router.push('/')}>Back Home</Button>}
            />
        </Row>

    );
};
export default PageNotfound;