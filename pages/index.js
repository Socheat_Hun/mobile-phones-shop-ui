import { Navbars } from "@/src/components/navbar/navbar";
import { Col, Row, Typography, Card, Rate, Descriptions } from 'antd';
import { Slider } from "@/src/components/slider/slider";

const { Title } = Typography;

const style = {
  background: '#0092ff',
  padding: '8px 0',
};

export default function App() {
  return (
    <>
      <Navbars />
      <div className="bgWhite">
        <Row className="containerLR" align="middle">
          <Col flex={2} >
            <Title className="titleHero">
              Every Purchase<br />will Be Made<br />With Pleasure
            </Title>
            <Title level={5}>Buying and selling of goods or services using the internet.</Title>
          </Col>
          <Col flex={3} className="textAlign">
            <img src="/banner.jpg" alt="banner" width={400} />
          </Col>
        </Row>
        <Slider />
        <Row gutter={16} justify='space-between' style={{ marginLeft: '15%', marginRight: '15%', marginTop: '2%' }}>
          <Col span={4} xs={24} xl={8} style={{ marginTop: '15px' }}>
            <Card bordered={false} bodyStyle={{ padding: '10px' }}>
              <Row>
                <Col>
                  <img src="https://imageio.forbes.com/specials-images/imageserve/637d5ab08792833e25c808be/0x0.png?format=png&crop=1324,853,x146,y0,safe&width=1200" alt="product" height={120} />
                </Col>
                <Col>
                  <Row>
                    {/* <Col>
                      <Title level={4}>iPhone 14 pro max</Title>
                    </Col> */}
 <Col>
                    <Descriptions title="User Info">
                      <Descriptions.Item label="UserName">Zhou</Descriptions.Item>
                    </Descriptions>
                  </Col>
                  </Row>
                  {/* <Col>
                    <Rate style={{}} />
                  </Col> */}
                 
                </Col>
              </Row>
            </Card>
          </Col>
          <Col span={4} xs={24} xl={8} style={{ marginTop: '15px' }}>
            <Card title="Card title" bordered={false}>
              Card content
            </Card>
          </Col>
          <Col span={4} xs={24} xl={8} style={{ marginTop: '15px' }}>
            <Card title="Card title" bordered={false}>
              Card content
            </Card>
          </Col>
          <Col span={4} xs={24} xl={8} style={{ marginTop: '15px' }}>
            <Card title="Card title" bordered={false}>
              Card content
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
}