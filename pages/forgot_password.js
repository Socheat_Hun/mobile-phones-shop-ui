import { Row, Col, Button, Checkbox, Form, Input, Avatar, View, Text, Layout } from 'antd';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router';

const { Header, Content, Footer } = Layout;
const url = 'https://www.seekpng.com/png/detail/58-588581_open-diseos-para-logos-png.png';


const ForgotPassword = () => {
    const router = useRouter()
    const onFinish = (values) => {
        console.log('Received values of form: ', values);
    };
    return (
        <Layout style={{ height: '100vh' }}>
            <Row className='homepage-bgimage' align="middle" style={{ height: '100vh' }}>
                <Col xs={{ span: 5, offset: 1 }} lg={{ span: 6, offset: 2 }}>
                </Col>
                <Col xs={{ span: 11, offset: 1 }} lg={{ span: 5, offset: 2 }}>
                    <div style={{ textAlign: 'center', marginBottom: '10px' }}>
                        <Avatar src={<img src={url} alt="avatar" />} size={65} />
                        <h3 style={{ color: '#595959' }}>Login to get all items.</h3>
                    </div>

                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                    >
                        <Form.Item
                            name="email"
                            rules={[
                                {
                                    type: 'email',
                                    message: 'The input is not valid E-mail!',
                                },
                                {
                                    required: true,
                                    message: 'Please input your E-mail!',
                                },
                            ]}
                        >
                            <Input placeholder="Email"/>
                        </Form.Item>

                        
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button" style={{ width: '100%' }}>
                                Reset Password
                            </Button>
                        </Form.Item>
                        <h4 style={{ width: '100%', textAlign: 'center', borderBottom: '1px solid #000', lineHeight: '0.1em', margin: '10px 0 20px', borderColor: '#595959' }}><span style={{ backgroundColor: 'whitesmoke', padding: '0 10px', color: '#595959' }}>OR</span></h4>
                        <Row>
                            <Button onClick={() => router.push('/login')} type="primary" className="login-form-button" style={{ width: '100%', backgroundColor: '#73d13d' }}>
                                Sign In
                            </Button>
                        </Row>
                    </Form>
                </Col>
                <Col xs={{ span: 5, offset: 1 }} lg={{ span: 6, offset: 2 }}>
                </Col>
            </Row>
            <Footer style={{ padding: '10px', textAlign: 'center', background: '#d9d9d9' }}>
                WinGo ©2023 Created by Hun Socheat
            </Footer>
        </Layout>
    )
};
export default ForgotPassword;