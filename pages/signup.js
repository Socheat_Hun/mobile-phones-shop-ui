import {
    AutoComplete,
    Button,
    Cascader,
    Checkbox,
    Col,
    Form,
    Input,
    InputNumber,
    Row,
    Select,
    Avatar,
    Layout
} from 'antd';
const { Header, Content, Footer } = Layout;
const url = 'https://www.seekpng.com/png/detail/58-588581_open-diseos-para-logos-png.png';
const { Option } = Select;
const residences = [
    {
        value: 'Phnom Penh',
        label: 'Phnom Penh',
        children: [
            {
                value: 'Sensok',
                label: 'Sensok',
                children: [
                    {
                        value: 'Teok thla',
                        label: 'Teok thla',
                    },
                    {
                        value: 'Phnom Penh thmey',
                        label: 'Phnom Penh thmey',
                    },
                ],
            },
        ],
    },
    {
        value: 'Takeo',
        label: 'Takeo',
        children: [
            {
                value: 'Angkor Borey',
                label: 'Angkor Borey',
                children: [
                    {
                        value: 'Basrae',
                        label: 'Basrae',
                    },
                ],
            },
        ],
    },
];
const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};
const SignUp = () => {
    const [form] = Form.useForm();
    const onFinish = (values) => {
        console.log('Received values of form: ', values);
    };
    const prefixSelector = (
        <Form.Item name="prefix" noStyle>
            <Select
                style={{
                    width: 80,
                }}
            >
                <Option value="855">+855</Option>
                <Option value="223">+223</Option>
                <Option value="364">+364</Option>
                <Option value="233">+233</Option>
                <Option value="647">+647</Option>
                <Option value="853">+853</Option>
                <Option value="199">+199</Option>
                <Option value="325">+325</Option>
                <Option value="743">+743</Option>
                <Option value="257">+257</Option>
            </Select>
        </Form.Item>
    );
    return (
        <Layout style={{height:'100vh'}}>
            <Row className='homepage-bgimage' align={'middle'} style={{ height: '100vh' }}>
                <Col xs={{ span: 5, offset: 1 }} lg={{ span: 4, offset: 2 }}>
                </Col>
                <Col xs={{ span: 11, offset: 1 }} lg={{ span: 7, offset: 2 }}>
                    <Row style={{ width: '100%' }}>
                        <Col xs={{ span: 0, offset: 1 }} sm={{ span: 4, offset: 2 }} md={{ span: 4, offset: 2 }} lg={{ span: 4, offset: 2 }}>
                        </Col>
                        <Col xs={{ span: 23, offset: 1 }} sm={{ span: 16, offset: 2 }} md={{ span: 16, offset: 2 }} lg={{ span: 16, offset: 2 }}>
                            <div style={{ textAlign: 'center', marginBottom: '10px' }}>
                                <Avatar src={<img src={url} alt="avatar" />} size={65} />
                                <h3 style={{ color: '#595959', marginBottom: '25px' }}>Sign In if you have Account. <a href='/login'>Sign In</a></h3>
                            </div>
                        </Col>
                    </Row>
                    <Form
                        {...formItemLayout}
                        layout="Horizontal"
                        form={form}
                        name="register"
                        onFinish={onFinish}
                        initialValues={{
                            residence: ['zhejiang', 'hangzhou', 'xihu'],
                            prefix: '855',
                        }}
                        style={{
                            maxWidth: 600,
                        }}
                        scrollToFirstError
                    >
                        <Form.Item
                            name="Username"
                            label="Username"
                            tooltip="What do you want others to call you?"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your username!',
                                    whitespace: true,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            name="email"
                            label="E-mail"
                            rules={[
                                {
                                    type: 'email',
                                    message: 'The input is not valid E-mail!',
                                },
                                {
                                    required: true,
                                    message: 'Please input your E-mail!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            label="Password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                            hasFeedback
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            name="confirm"
                            label="Confirm Password"
                            dependencies={['password']}
                            hasFeedback
                            rules={[
                                {
                                    required: true,
                                    message: 'Please confirm your password!',
                                },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                        if (!value || getFieldValue('password') === value) {
                                            return Promise.resolve();
                                        }
                                        return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                    },
                                }),
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>
                        <Form.Item
                            name="Address"
                            label="Address"
                            rules={[
                                {
                                    type: 'array',
                                    required: true,
                                    message: 'Please select your address!',
                                },
                            ]}
                        >
                            <Cascader options={residences} />
                        </Form.Item>

                        <Form.Item
                            name="phone"
                            label="Phone Number"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your phone number!',
                                },
                            ]}
                        >
                            <Input
                                addonBefore={prefixSelector}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Form.Item>
                        <Form.Item
                            name="gender"
                            label="Gender"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please select gender!',
                                },
                            ]}
                        >
                            <Select placeholder="select your gender">
                                <Option value="male">Male</Option>
                                <Option value="female">Female</Option>
                                <Option value="other">Other</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item
                            name="agreement"
                            valuePropName="checked"
                            rules={[
                                {
                                    validator: (_, value) =>
                                        value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
                                },
                            ]}
                            {...tailFormItemLayout}
                        >
                            <Checkbox>
                                I have read the <a href="">agreement</a>
                            </Checkbox>
                        </Form.Item>
                        <div>
                            <Row style={{ width: '100%' }}>
                                <Col xs={{ span: 0, offset: 1 }} sm={{ span: 4, offset: 2 }} md={{ span: 4, offset: 2 }} lg={{ span: 4, offset: 2 }}>
                                </Col>
                                <Col xs={{ span: 23, offset: 1 }} sm={{ span: 16, offset: 2 }} md={{ span: 16, offset: 2 }} lg={{ span: 16, offset: 2 }}>
                                    <Button type="primary" htmlType="submit" style={{ width: '100%' }}>
                                        Register
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </Form>
                </Col>
                <Col xs={{ span: 5, offset: 1 }} lg={{ span: 6, offset: 2 }}>
                </Col>
            </Row>
            <Footer  style={{padding:'10px',textAlign:'center',background:'#d9d9d9'}}>
            WinGo ©2023 Created by Hun Socheat
            </Footer>
        </Layout>
    );
};
export default SignUp;
